(def game-world
  '((::base ::text ::base nil    nil    nil    nil    nil    nil)
    (::ntro nil    ::text ::boss ::text ::item ::text ::base ::base)
    (::item ::text ::base nil    nil    nil    ::goal nil    ::keyf)))

(def directions 
  {"North" '(-1 0) 
   "East" '(0 1) 
   "South" '(1 0) 
   "West" '(0 -1)})

(def ascii-keyf " 
   888  888  888
   888  888  888

   888       888  888  888  888  888  888  888
   888       888  888  888  888  888  888  888

   888  888  888                 888       888
   888  888  888                 888       888")

(def script 
  {:move-before        "She decided to move... "
   :move-after1        "Adrianne headed "
   :move-after2        " and found herself...\n"
   :health-loss        "Adrianne's arm was bleeding badly..."
   :health-game-over  ["Adrianne was struggling to put one foot in front of the other."
                       "She had lost too much blood. This was it."
                       "She stumbled and fell flat on her face but she didn't even feel any pain."
                       "All she could think about..."
                       "was how..."
                       "tired..."
                       "she was..."
                       "\n\n --GAME OVER--\n"]
   :monster-far       ["The creature's footsteps were echoing through the dungeon."]
   :monster-near      ["The dungeon was eerily quiet."]
   :monster-game-over ["The dungeon was still eerily quiet."
                       "Something about the silence was very strange. Where had the creature gone to?"
                       "There was a quiet, almost imperceptible sound behind her and suddenly Adrianne realized."
                       "\nThe loud roars, the heavy footsteps...\nThe creature was making these noises on purpose, to make its preys associate them with its presence."
                       "So that then it would just have to move stealthily, and its preys would be quick to lower their guard.\nAnd right at that moment..."
                       "\nAdrianne spun around but it was already far too late. The creature was upon her, ready to strike."
                       "There was a lightning-fast slash of claws and it was all over."
                       "The last thing Adrianne saw before her vision faded to black was the creature's glowing eye starring at her in the darkness."
                       "\n\n --GAME OVER--\n"]
   :monster-asleep    ["...in the creature's room. The creature seemed to be sound asleep."
                       "\nAs she moved closer, Adrianne noticed that the cuffs connected to the creature's chains had a keyhole on them."
                       "Her golden key was a perfect fit."]
   :monster-stare     ["...in the creature's room. To the west and to the east lied two large stone doorways."
                       "The creature was tied to a pole by a sturdy-looking golden chain, but it didn't even attempt to move and reach for Adrianne."
                       "Instead it remained absolutely still as it watched her make her way across the room."]
   :monster-gone      ["...in the creature's room. But the creature itself was gone."]
   :monster-prompt     "Free the creature?"
   :monster-freed     ["\nIn one swift movement, Adrianne reached for the cuffs and used her key to unlocked them."
                       ascii-keyf
                       "\n\nThe cuffs fell to the ground with a silence-shattering clang."
                       "The creature stirred briefly but didn't wake up.\nAdrianne backed away and let out a breath she hadn't realized she had been holding."]
   :monster-not-freed ["\nNo, of course not. The very idea of freeing this monster was absolutely insane."
                       "How could she even have considered doing such a thing in the first place?..."]
   :secret-found      ["Adrianne inspected the southern wall.\nSome door hinges were camouflaged between the bricks and fresh air was seeping through."
                       "There seemed to be a large door hidden in the wall."
                       "\nUnable to find any mechanism to operate it, Adrianne threw herself at the door with all her strength."
                       "It rattled with a metallic sound but didn't give in. She simply wasn't strong enough to force it open."]
   :secret-locked     ["\nSomewhere, the creature roared again..."
                       "\nAdrianne hadn't moved so she was still..."]
   :ending            ["\nBut maybe..."
                       "\nFrom deep within the dungeon, the creature roared again."
                       "Adrianne gathered what little strength she had left and answered with a roar of her own.\nA roar that was desperate but brave. Broken but defiant. A roar that said \"I AM NOT AFRAID. COME AND GET ME.\""
                       "\nAt first the dungeon fell deathly quiet, but eventually the creature did come, it's glowing eye piercing the darkness."
                       "\nFor a long moment, Adrianne and the creature stood absolutely still as they starred each other down.\nThen, very slowly, the creature drew its fangs out and wound up its body like a spring."
                       "\nThere was a loud thunder-like bang, and in a blur of dark scales, it charged straight at Adrianne, aiming for a quick kill."
                       "But just at the last moment, she dodged, sidestepping the creature's attack in a single, swift motion."
                       "Taken completely by surprise, it was helpless to stop its own momentum."
                       "The creature crashed into the hidden door with such force that it was torn right off its hinges."
                       "\nHer way out was now finally in sight, but Adrianne wasn't free yet.\nThe creature had recovered very quickly and it was now standing between her and the exit, glaring at her menacingly."
                       "A sharp hinge fragment was lying by her feet. Slowly and carefully, she picked it up.\nClenching the makeshift blade tightly in her left hand, Adrianne prepared herself to fight a battle she knew she could not win."
                       "\nSuddenly a warm breeze filled the room, carrying with it the smells of summer."
                       "Sunlight peered through the newly-formed hole in the wall. Outside, the sun was rising."
                       "\nAs if having suddenly lost interest in the fight, the creature stepped outside and gazed toward the rising sun."
                       "After giving one last look at Adrianne, it walked away and vanished into the morning light."
                       "\nAdrianne felt the tension leave her body all at once and she fell to her knees."
                       "\nAfter a long while, Adrianne finally found the strength to get up."
                       "She left the golden key behind and stumbled out of the dungeon with a smile on her face."
                       "\n--END--\n"]
   :separator          "------------------------------------------------------"                     
                        
   ['(0 0) :default]  ["...in a staircase overrun by vegetation.\nIt came to a small landing before taking a sharp quarter turn."]
   ['(1 0) :before]   ["Adrianne was awoken by the loud clang of the dungeon's gate closing behind her."
                       "\nAs she staggered to her feet, she felt bolts of pain rush through her right arm.\nThe flesh had been torn in such an unnatural way and it was bleeding so profusely that it barely even looked like an arm anymore."
                       "With every passing moment, her breath was getting more ragged, her head dizzier. She was dying.\nShe needed to get out of this place, and quickly."
                       "\nThe dungeon's gate was very small -the average person had to lower their head to pass through- but it was incredibly sturdy.\nNo matter how hard Adrianne threw herself at it, the gate wouldn't budge."
                       "\nSuddenly, a deep, ominous roar shook the dungeon, making the torches flicker and the steel gate rattle."
                       "\n..."
                       "\nTrying her best to swallow her fear, Adrianne resolved to venture deeper into the dungeon."
                       "She wasn't exactly optimistic about her chances at finding another way out of this place.\nBut at the very least, if she was going to die, she wanted to die looking for one."
                       "\nFilled with that strange sort of courage that is born only from the darkest depths of despair, Adrianne turned her gaze forward.\n\nShe was..."]
   ['(1 0) :default]  ["...at the dungeon's entrance.\nThe small metal gate was locked tight."
                       "Branching out from the sides of the room were two stone staircases:\nOne going northward and upward, the other going southward and downward."]
   ['(2 0) :default]  ["...in a winding staircase.\nThe body of an man lied sprawled across the steps like a broken doll."]
   ['(2 0) :after]    ["\nAdrianne looted the corpse and found some fresh bandages.\nShe used them to patch up her wounded arm."]
   ['(0 1) :default]  ["...in a narrow, slightly flooded corridor."
                       "\nAdrianne's footsteps' echoed off the damp walls as she trudged through the water."]
   ['(0 1) :after]    ["\"Quiet!\" came a voice from behind her, nearly startling her to death.\nIt was an old man slouched against a wall. He had been so still that Adrianne had mistaken him for a corpse."
                       "\"Be quiet, you f-fool!\" The man warned again."
                       "\"T-This thing, it hunts its preys by sound. And b-believe me,\" He added, his pale face distorted by fear.\n\"You don't want it to hunt you.\""]
   ['(2 1) :default]  ["...in a long corridor littered with bloody footprints."]
   ['(2 1) :after]    ["\nThe prints were so big and so strangely shaped that Adrianne had no idea what they could have belonged to."
                       "Whatever it was, it seemed to have headed east."]
   ['(0 2) :default]  ["...at a bend in the corridor."]
   ['(1 2) :default]  ["...in a spacious hall. A young boy with silver hair stood in a corner, playing with rocks."
                       "\nTo the south and to the north, two hallways were going off in opposite directions.\nMeanwhile, to the east, a large stone staircase was leading deeper into the dungeon."]
   ['(1 2) :after]    ["The staircase led down to a small room at the center of which lied an imposing bronze statue."
                       "\nAs the statue turned its head, Adrianne realized with terror that the statue wasn't a statue at all."
                       "\nIt was a gigantic beast, with the head of a lion and a body covered in pitch-black scales.\nBut its most striking feature by far was its single, glowing eye."
                       "\nThe creature was in every way alien to Adrianne but the look in its eye felt eerily familiar to her."
                       "It was the look of a hunter assessing its prey."
                       "\n\"Don't worry, it's chained to a pole right now.\" A voice came interrupting her thoughts. It was the white-haired boy.\nHe must have seen the fear on her face, Adrianne thought."
                       "\"As long as you stay out of its reach, you should be able to cross the room safely.\"\nHe continued, absentmindedly juggling with a rock in his right hand."
                       "\nThe boy's words and his confident attitude didn't to much to alleviate the uneasy feeling Adrianne had in her stomach.\nBut she couldn't turn back now. She had to press forward, no matter what."]
   ['(2 2) :default]  ["...in a corridor that took a sharp quarter turn."]
   ['(1 4) :default]  ["...in a small passage overrun by moss."
                       "\nOn the side, a man and a woman were whispering to each other."]
   ['(1 4) :after]    ["\"Don't ya think it's mighty weird? For that monster to be so big?\" Whispered the woman, gesturing towards the creature's room."
                       "\"Nah, I reckon it wouldn't be a proper monster if it wasn't big, ya know?\" Countered the man, shrugging."
                       "\"No, no. I mean, remember the dungeon's gate?\nThere's no way in hell a beast that big would fit through a tiny gate like that.\" Continued the woman, scratching her chin."
                       "\"So what? Don't that just mean that it was still small when it was bringed here?\""
                       "\"Maybe that's it. Or maybe...\" She trailed off."]
   ['(1 5) :default]  ["...in a corridor.\nOn the side, there was some sort of small alcove where one could sit."]
   ['(1 5) :after]    ["Adrianne sat down to take a breather and tended to her wounds a bit."]
   ['(1 6) :default]  ["...in a small room with a cracked ceiling. Moonlight was shining through."]
   ['(1 6) :after]    ["\nThis glimpse of the outside world filled Adrianne with determination."]
   ['(1 7) :default]  ["...in a straight corridor."
                       "A message had been written on the wall with blood.\n[CHOOSING A PATH THAT CANNOT BE CHOSEN]\n      [THEREIN LIES TRUE FREEDOM]"]
   ['(1 8) :default]  ["...at a bend in the corridor."
                       "The walls were covered with more bloody writings.\nOne sentence seemed to have been written over and over everywhere.\n[TO FIND ONE'S WAY]\n  [A MAP IS KEY]"]
   ['(2 8) :default]  ["...in a small cell."]
   ['(2 8) :after]    ["\nAs Adrianne stepped into the room, the first thing she noticed was the nauseating smell that filled the air."
                       "Only then did she notice where it was originating from. There at the center of the cell, someone had hanged themselves.\nThe body was well into the late stages of decomposition, but Adrianne could tell that it belonged to a young, red-haired girl."
                       "\nAfter a moment, Adrianne managed to regain her composure and decided to take a closer look at the corpse."
                       "There were huge claw marks on her stomach and parts of her right leg had been bitten off.\nIn her left hand, the girl was holding something."
                       ascii-keyf
                       "\n\nIt was a small golden key. Adrianne pocketed it."
                       "\"Ah, that key.\" A familiar voice came from the doorway. It was the silver-haired boy again."
                       "\n\"I would be careful with it if I were you. I still don't know where she found this key. But after she did,\" He paused briefly\nand looked down at the rock in his hand, as if to hide the emotions flickering across his face."
                       "\"After she did, she started going... crazy.\" He continued, turning his gaze back to Adrianne.\n\"The writings on the walls, they're all hers. With her own blood, too.\""
                       "\nThe boy had been clenching the rock in his hand so hard that his knuckles had turned white.\n\"Her name was Sarah.\" He said after a long silence. \"She was my friend.\""]})

(def hp-start 10)
(def hp-heal 12)
(def danger-start 6) 



;; TEXT DISPLAY

(defn display [message]
  (do (print message) (flush)))

(defn text-seq [[start & end]]
  (when start
    (display (str start " "))
    (read-line)
    (recur end)))

(defn prompt [question]
  (do (display question)
      (display "\n[y] Yes\n[N] no\n")
      (let [inpuf (read-line)]
        ((complement not-any?) #(= inpuf %) '("y" "Y")))))

(defn play-text-seqs [position timings flags]
  (if-not (empty? timings)
    (do (text-seq (script [position (first timings)]))
        (recur position (rest timings) flags))
    (do (newline) flags)))


;; GAME WORLD

(defn coord [position] 
  (-> game-world 
      (nth (first position) nil) 
      (nth (second position) nil)))

(defn apply-coords [position1 position2] 
  (map + position1 position2)) 

(defn room-connections [position]
  (loop [checks directions connections {}]
    (if (empty? checks)
      connections
      (let [checking (apply-coords position (val (first checks)))
            checks-left (dissoc checks (key (first checks)))]
        (if (coord checking)
          (recur checks-left (assoc connections (key (first checks)) checking))
          (recur checks-left connections))))))


;; MOVEMENT

(defn initial [string]
  (str (clojure.string/capitalize (first string))))   

(defn hide-goal [connections]
  (let [goal (::goal (zipmap (map coord (vals connections)) (keys connections)))]
    (if-not goal
      connections
      (dissoc connections goal)))) 

(defn move-options [connections]
  (->> (hide-goal connections)
       (keys)
       (map #(str "[" (initial %) "] " % "\n"))
       (apply str)))

(defn move-prompt [connections]
  (let [inpuf (clojure.string/capitalize (read-line)) 
        match (->> (keys connections)
              (map #(if (= (initial %) inpuf) % ""))
              (apply str))]
    (if-not (empty? match)
      (find connections match)
      (recur connections))))

(defn narrate-move? [hp danger movement]
  (and (not= ::goal (coord (val movement)))
       (> hp 0)
       (> danger 0)))

(defn move [hp danger connections]
  (do (display (script :separator))
      (newline) (newline)
      (display (script :move-before))
      (newline)
      (display (move-options connections))
      (let [movement (move-prompt connections)
            direction (clojure.string/lower-case(key movement))] 
        (do (newline)
            (if (narrate-move? hp danger movement)
              (display (str (script :move-after1) direction (script :move-after2))))
            (val movement)))))


;; ROOM        
          
(defn room [position room-type flags]
  (if (= room-type ::base)
    (play-text-seqs position [:default] flags)
    (if (contains? flags position)
      (play-text-seqs position [:default] flags)
      (let [new-flags (assoc flags position room-type)]
        (if (= room-type ::ntro)
          (play-text-seqs position [:before :default] new-flags)
          (play-text-seqs position [:default :after] new-flags))))))

(defn monster-chained [position room-type flags]
  (if-not (some #{::keyf} (vals flags))
    (do (text-seq (script :monster-stare))
        (newline)
        flags)
    (do (text-seq (script :monster-asleep))
        (if-not (prompt (script :monster-prompt))
          (do (text-seq (script :monster-not-freed))
              (newline)
              flags)
          (do (text-seq (script :monster-freed))
              (newline)
              (assoc flags position room-type))))))

(defn monster-room [position room-type flags]
  (if-not (some #{::boss} (vals flags))
    (monster-chained position room-type flags)
    (do (text-seq (script :monster-gone))
        (newline)
        flags)))

(declare room-flow)

(defn goal-room [flags hp danger position]
  (do (text-seq (script :secret-found))
      (if-not (some #{::boss} (vals flags))
        (do (text-seq (script :secret-locked))
            (newline)
            (room-flow flags hp danger position))
        (text-seq (script :ending)))))

(defn room-handler [position room-type flags]
  (if (= room-type ::boss)
    (monster-room position room-type flags)
    (room position room-type flags)))


;; HEALTH

(defn change-hp [old-hp new-hp]
  (do (text-seq [(str "Health: " old-hp " -> " new-hp)])
      (newline)
      new-hp))

(defn health [position room-type flags hp]
  (if (and (= room-type ::item) (not (contains? flags position)))
    (change-hp hp (+ hp hp-heal))
    (do (display (script :health-loss))
        (newline)
        (change-hp hp (dec hp)))))


;; MONSTER

(defn monster [flags danger]
  (if (some #{::boss} (vals flags))
    (do (if (< (dec danger) (/ danger-start 3))
          (text-seq (script :monster-near))
          (text-seq (script :monster-far)))
        (newline)
        (dec danger))
    danger))
        
        
;; MAIN

(defn room-flow [flags hp danger position]
  (let [room-type (coord position)
        new-flags (room-handler position room-type flags)
        new-hp (health position room-type flags hp)
        new-danger (monster flags danger)
        new-position (move new-hp new-danger (room-connections position))]
    (cond (< new-hp 1)
          (text-seq (script :health-game-over))
          (< new-danger 1)
          (text-seq (script :monster-game-over))
          (= ::goal (coord new-position))
          (goal-room new-flags new-hp new-danger position)
          :else (recur new-flags new-hp new-danger new-position))))
          

(defn the-roar
  "A text-based game about a girl trapped in a dungeon with a dangerous creature.
  Minimum terminal size: 132 collumns.
  Type (the-roar) to play."
  []
  (do (newline)
      (room-flow {} hp-start danger-start '(1 0))))      

