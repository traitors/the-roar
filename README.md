# The Roar

You can load the game in a REPL with `(clojure.main/load-script "TheRoar.clj")`.
Next, you launch the game by calling the `(the-roar)` procedure.
